#!/usr/bin/python3
#
# Copyright (C) 2014-2017 Johannes Schauer <j.schauer@email.de>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software. 

import subprocess
import yaml
import daklib_arch
import debarch
import tempfile
import os
import random

abi_from_abitable = [row[0] for row in debarch._load_table('/usr/share/dpkg/abitable')]
abi_from_ostable = [row[0].split('-')[0] for row in debarch._load_table('/usr/share/dpkg/ostable')]
abi_from_tupletable = [row[0].split('-')[0] for row in debarch._load_table('/usr/share/dpkg/tupletable')]
abi_wildcards = sorted(set(abi_from_abitable+abi_from_ostable+abi_from_tupletable+['any']))

libc_from_ostable = [row[0].split('-')[1] for row in debarch._load_table('/usr/share/dpkg/ostable')]
libc_from_tupletable = [row[0].split('-')[1] for row in debarch._load_table('/usr/share/dpkg/tupletable')]
libc_wildcards = sorted(set(libc_from_ostable+libc_from_tupletable+['any']))

os_from_ostable = [row[0].split('-')[2] for row in debarch._load_table('/usr/share/dpkg/ostable')]
os_from_tupletable = [row[0].split('-')[2] for row in debarch._load_table('/usr/share/dpkg/tupletable')]
os_wildcards = sorted(set(os_from_ostable+os_from_tupletable+['any']))

cpu_from_cputable = [row[0] for row in debarch._load_table('/usr/share/dpkg/cputable')]
cpu_from_tupletable = [row[0].split('-')[3] for row in debarch._load_table('/usr/share/dpkg/tupletable')]
cpu_wildcards = sorted(set(cpu_from_cputable+cpu_from_tupletable+['any'])-set(['<cpu>']))

wildcard_list = list()

for c in cpu_wildcards:
    wildcard_list.append(c)
    for o in os_wildcards:
        wildcard_list.append(o+'-'+c)
        for l in libc_wildcards:
            wildcard_list.append(l+'-'+o+'-'+c)
            for a in abi_wildcards:
                wildcard_list.append(a+'-'+l+'-'+o+'-'+c)

debarches = subprocess.check_output(["dpkg-architecture", "-L"]).decode().split()

def dpkg_arch_matches(arch, wildcard):
    # environment must be empty or otherwise the DEB_HOST_ARCH environment
    # variable will influence the result
    return subprocess.call(
        ['dpkg-architecture', '-i%s' % wildcard, '-a%s' % arch],
        env={}) == 0

def dose_arch_matches(arch, wildcard):
    f1 = tempfile.NamedTemporaryFile(mode='w', delete=False)
    n1 = f1.name
    f1.write("""
Package: foo
Architecture: %s
Version: 0.invalid.0
"""%(wildcard))
    f1.close()
    f2 = tempfile.NamedTemporaryFile(mode='w', delete=False)
    n2 = f2.name
    f2.write("""
Package: build-essential
Architecture: %s
Version: 0.invalid.0
"""%(arch))
    f2.close()
    data = subprocess.check_output(['dose-builddebcheck', '--deb-native-arch=%s'%arch,
        '--successes', '/tmp/packages', '/tmp/sources'])
    os.unlink(n1)
    os.unlink(n2)
    data = yaml.load(data, Loader=yaml.CBaseLoader)
    return len(data['report']) == 1

check_pairs = [ (d,w) for d in debarches for w in wildcard_list ]

random.seed(0) # to reproduce the same random sample
check_pairs = random.sample(check_pairs, int(len(check_pairs)/1000))

len_check_pairs = len(check_pairs)

print("checking %d testcases"%len_check_pairs)

for i,(d,w) in enumerate(check_pairs):
    print("\r%f"%((i*100)/len_check_pairs), end="")
    dose_res = dose_arch_matches(d, w)
    dpkg_res = dpkg_arch_matches(d, w)
    try:
        dak_res = daklib_arch.match_architecture(d, w)
    except daklib_arch.InvalidArchitecture:
        dak_res = False
    deb_res = debarch.match_architecture(d,w)
    if dose_res != dpkg_res or dose_res != dak_res \
            or dose_res != deb_res:
        print("\ndifference!")
        print("dose: %s matches %s: %s"%(w,d,dose_res))
        print("dpkg: %s matches %s: %s"%(w,d,dpkg_res))
        print("deb:  %s matches %s: %s"%(w,d,deb_res))
        print("dak:  %s matches %s: %s"%(w,d,dak_res))
